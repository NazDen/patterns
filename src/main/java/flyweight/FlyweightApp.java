package flyweight;

import java.util.*;

public class FlyweightApp {

    public static void main(String[] args) {

        ShapeFactory shapeFactory = new ShapeFactory();
        List<Shape> shapes = new ArrayList<>() {{
            add(shapeFactory.getShape("square"));
            add(shapeFactory.getShape("square"));
            add(shapeFactory.getShape("square"));
            add(shapeFactory.getShape("point"));
            add(shapeFactory.getShape("point"));
            add(shapeFactory.getShape("point"));
            add(shapeFactory.getShape("point"));

        }};
        Random random = new Random();
        shapes.forEach(shape -> shape.draw(random.nextInt(100), random.nextInt(100)));
    }

}

interface Shape {
    void draw(int x, int y);
}

class Point implements Shape {

    public void draw(int x, int y) {
        System.out.println(String.format("x= %d y= %d, point", x, y));
    }
}

class Square implements Shape {
    int a = 5;

    public void draw(int x, int y) {
        System.out.println(String.format("x= %d y= %d side= %d, square", x, y, a));
    }
}

class ShapeFactory {
    private Map<String, Shape> shapes = new HashMap<>();

    public void showShapes(){

    }

    public Shape getShape(String type) {
        Shape shape = shapes.get(type);
        if (shape == null) {
            if (type.equals("point")) {
                shape = new Point();
            } else if (type.equals("square")) {
                shape = new Square();
            }
            shapes.put(type, shape);
        }
        return shape;
    }


}

