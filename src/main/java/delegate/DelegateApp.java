package delegate;

public class DelegateApp {
    public static void main(String[] args) {

        Child child = new Child();
        child.setToy(new Train());
        child.play();
        child.setToy(new Car());
        child.play();

    }
}

interface Toy {
    void play();
}

class Car implements Toy {
    public void play() {
        System.out.println("Playing with car");
    }
}

class Train implements Toy {
    public void play() {
        System.out.println("Playing with train");
    }
}

class Child {
    private Toy toy;

    void setToy(Toy toy) {
        this.toy = toy;
    }

    void play() {
        toy.play();
    }
}