package decorator;

public class DecoratorApp {
    public static void main(String[] args) {

        PrintInterface squareBracketsDecorator = new SquareBracketsDecorator(new Component());
        squareBracketsDecorator.print();

    }
}

interface PrintInterface{
    void print();
}

class Component implements PrintInterface{

    public void print() {
        System.out.print("Component");
    }
}

class SquareBracketsDecorator implements PrintInterface{

    PrintInterface component;

    public SquareBracketsDecorator(PrintInterface component) {
        this.component = component;
    }

    public void print() {
        System.out.print("[");
        component.print();
        System.out.print("]");
    }
}


