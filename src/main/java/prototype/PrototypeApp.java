package prototype;

public class PrototypeApp {

    public static void main(String[] args){

        Car toyota = new Car("Toyota", 200);
        Car copyT =(Car)toyota.clone();
        System.out.println(toyota.toString());
        System.out.println(copyT.toString());

    }

}

interface Copiable{
    Object clone();
}

class Car implements Copiable{

    private String name;
    private int maxSpeed;

    public Car(String name, int maxSpeed) {
        this.name = name;
        this.maxSpeed = maxSpeed;
    }


    @Override
    public Object clone() {
        Car copy = new Car(name, maxSpeed);
        return copy;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", maxSpeed=" + maxSpeed +
                '}';
    }
}