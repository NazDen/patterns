package proxy;

public class ProxyApp {

    public static void main(String[] args) {

        Image image = new ProxyImage("D:/image/my.jpg");
        image.display();

    }

}

class ProxyImage implements Image {
    String file;
    RealImage image;

    public ProxyImage(String path) {
        this.file = path;
    }

    public void display() {
        if (image == null){
            image = new RealImage(file);
        }
        image.display();
    }
}

interface Image {
    void display();
}

class RealImage implements Image {

    String file;

    public RealImage(String path) {
        this.file = path;
        load();
    }

    void load() {
        System.out.println(String.format("%s loading", file));
    }

    public void display() {
        System.out.println(String.format("Watch %s", file));
    }
}