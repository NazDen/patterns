package singleton;

public class SingletonApp {
    public static void main(String[] args) throws InterruptedException {
        final int SIZE = 100;
        Thread[] threads = new Thread[SIZE];

        for (int i = 0; i < SIZE; i++) {
            threads[i] = new Thread(new R());
            threads[i].start();
        }

        for (int i = 0; i < SIZE; i++) {
            threads[i].join();
        }
        System.out.println(Singleton.counter);

    }
}

class R implements Runnable {

    public void run() {
        Singleton.getInstance();
    }
}

class Singleton {

    private static volatile Singleton instance;
    public static int counter;

    private Singleton() {
        counter++;
    }

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}

