package composite;

import java.util.ArrayList;
import java.util.List;

public class CompositeApp {
    public static void main(String[] args) {

        Composite composite = new Composite(){{
            addToy(new Robot());
            addToy(new Car());
            addToy(new Train());
            play();
        }};
    }
}
interface Toy{
    void play();
}

class Car implements Toy{

    public void play() {
        System.out.println("Playing with car");
    }
}

class Train implements Toy{
    public void play() {
        System.out.println("Playing with train");
    }
}

class Robot implements Toy{

    public void play() {
        System.out.println("Playing with robot");
    }
}

class Composite implements Toy{
    private List<Toy> toys = new ArrayList<>();

    void addToy(Toy toy){
        toys.add(toy);
    }

    void removeToy(Toy toy){
        toys.remove(toy);
    }

    public void play() {
        toys.forEach(Toy::play);
    }
}