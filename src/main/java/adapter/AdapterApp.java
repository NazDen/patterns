package adapter;

public class AdapterApp {

    public static void main(String[] args) {

        // inheritance
        Toy barbie = new ToyAdapterFromWoodBarbie();
        barbie.play();

        // composition
        Toy barbie2 = new ToyAdapterFromWoodBarbie2();
        barbie2.play();
    }

}

interface Toy {
    void play();
}

class WoodBarbie {
    void playWithWooDBarbie() {
        System.out.println("Playing with wood barbie");
    }
}

class ToyAdapterFromWoodBarbie extends WoodBarbie implements Toy {

    public void play() {
        playWithWooDBarbie();
    }
}

class ToyAdapterFromWoodBarbie2 implements Toy {
    WoodBarbie barbie = new WoodBarbie();

    public void play() {
        barbie.playWithWooDBarbie();
    }
}



