package bridge;

public class BridgeApp {

    public static void main(String[] args) {

        Sedan sedan = new Sedan(new Skoda());
        sedan.showDetails();

    }

}

abstract class Car {
    Make make;

    public Car(Make make) {
        this.make = make;
    }
    abstract void showType();
    void showDetails(){
        showType();
        make.showMake();
    }

}

class Sedan extends Car {

    public Sedan(Make make) {
        super(make);
    }

    void showType() {
        System.out.println("Sedan");
    }
}

class Hatchback extends Car {
    public Hatchback(Make make) {
        super(make);
    }

    void showType() {
        System.out.println("Hatchback");

    }
}

interface Make {
    void showMake();
}

class Skoda implements Make {


    public void showMake() {
        System.out.println("Skoda");
    }
}

class Mazda implements Make {


    public void showMake() {
        System.out.println("Mazda");
    }
}