package facade;

public class FacadeApp {

    public static void main(String[] args) {

        Computer computer = new Computer();
        computer.copy();

    }

}

class Power {
    void on() {
        System.out.println("On");
    }

    void off() {
        System.out.println("Off");
    }
}

class Computer {
    Power power = new Power();
    DVDRom dvdRom = new DVDRom();
    HDD hdd = new HDD();

    void copy() {
        power.on();
        dvdRom.load();
        hdd.copyFromDVD(dvdRom);
    }
}

class DVDRom {
    private boolean data;

    public boolean hasData() {
        return data;
    }

    public void load() {
        data = true;
    }
}

class HDD {
    void copyFromDVD(DVDRom dvdRom) {
        if (dvdRom.hasData()) {
            System.out.println("Copping from DVD");
        } else {
            System.out.println("DVD not found");
        }
    }
}

