package abstractFactory;

import abstractFactory.toys.*;
import abstractFactory.toys.plastic.PlasticBarbie;
import abstractFactory.toys.plastic.PlasticCar;
import abstractFactory.toys.plastic.PlasticLego;
import abstractFactory.toys.plastic.PlasticTrain;
import abstractFactory.toys.wood.WoodCar;
import abstractFactory.toys.wood.WoodTrain;
import enums.MaterialTypes;
import enums.ToyTypes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public class AbstractFactoryApp {

    public static void main(String[] args) {

        Stream.of(MaterialTypes.values()).filter(materialType -> !materialType.equals(MaterialTypes.UNKNOWN))
                .map(materialType -> getByMaterialType(materialType.name())).map(toyFactory -> {
            List<Toy> toys = new ArrayList<>();
            for (ToyTypes toyType :
                    ToyTypes.values()) {
                try {
                    toys.add(toyFactory.createToy(toyType.name()));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            return toys;
        }).flatMap(Collection::stream).forEach(Toy::play);

    }

    private static ToyFactory getByMaterialType(String materialType) {
        if (materialType.equalsIgnoreCase(MaterialTypes.PLASTIC.name())) {
            return new PlasticToyFactory();
        } else if (materialType.equalsIgnoreCase(MaterialTypes.WOOD.name())) {
            return new WoodToyFactory();
        } else {
            throw new IllegalArgumentException(String.format("%s doesnt available.", materialType));
        }
    }

}

interface ToyFactory {
    Toy createToy(String toyType);
}

class WoodToyFactory implements ToyFactory {

    public Toy createToy(String toyType) {
        if (toyType.equalsIgnoreCase(ToyTypes.CAR.name()))
            return new WoodCar();
        else if (toyType.equalsIgnoreCase(ToyTypes.TRAIN.name()))
            return new WoodTrain();
        else
            throw new IllegalArgumentException(String.format("%s doesnt available.", toyType));
    }
}

class PlasticToyFactory implements ToyFactory {

    public Toy createToy(String toyType) {
        if (toyType.equalsIgnoreCase(ToyTypes.CAR.name()))
            return new PlasticCar();
        else if (toyType.equalsIgnoreCase(ToyTypes.TRAIN.name()))
            return new PlasticTrain();
        else if (toyType.equalsIgnoreCase(ToyTypes.LEGO.name()))
            return new PlasticLego();
        else if (toyType.equalsIgnoreCase(ToyTypes.BARBIE.name()))
            return new PlasticBarbie();
        else
            throw new IllegalArgumentException(String.format("%s doesnt available.", toyType));
    }
}
