package abstractFactory.toys.plastic;

public final class PlasticCar extends PlasticToy {
    private final String name = "car";
    public void play() {
        System.out.println(String.format("%s %s %s.", play, materialName, name));
    }
}
