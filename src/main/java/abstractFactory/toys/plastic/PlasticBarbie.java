package abstractFactory.toys.plastic;

public class PlasticBarbie extends PlasticToy{
    private final String name = "barbie";
    public void play() {
        System.out.println(String.format("%s %s %s.", play, materialName, name));
    }
}
