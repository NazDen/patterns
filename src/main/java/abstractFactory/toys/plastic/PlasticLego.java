package abstractFactory.toys.plastic;

public class PlasticLego extends PlasticToy {
    private final String name = "lego";
    public void play() {
        System.out.println(String.format("%s %s %s.", play, materialName, name));
    }
}
