package abstractFactory.toys.plastic;

public class PlasticTrain extends PlasticToy {
    private final String name = "train";
    public void play() {
        System.out.println(String.format("%s %s %s.", play, materialName, name));
    }
}
