package abstractFactory.toys.plastic;

import abstractFactory.toys.Toy;

public abstract class PlasticToy implements Toy {
    protected final String materialName = "plastic";

    public PlasticToy() {
    }

    public abstract void play();
}
