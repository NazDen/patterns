package abstractFactory.toys.wood;

public final class WoodCar extends WoodToy {
    private final String name = "car";


    public void play() {
        System.out.println(String.format("%s %s %s.", play, materialName, name));
    }
}
