package abstractFactory.toys.wood;
import abstractFactory.toys.Toy;

public abstract class WoodToy implements Toy {
    protected final String materialName = "wood";

    public WoodToy() {
    }

    public abstract void play();

}
