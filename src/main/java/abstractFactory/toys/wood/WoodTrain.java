package abstractFactory.toys.wood;

public final class WoodTrain extends WoodToy {
    protected final String name = "train";

    public void play() {
        System.out.println(String.format("%s %s %s.", play, materialName, name));
    }
}
