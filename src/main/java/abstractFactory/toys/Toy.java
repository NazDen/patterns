package abstractFactory.toys;

public interface Toy {

    String play = "Playing with";
    void play();

}
