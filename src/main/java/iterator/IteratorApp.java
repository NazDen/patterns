package iterator;

public class IteratorApp {

    public static void main(String[] args) {

        ArrayTasks c = new ArrayTasks();
        Iterator iterator = c.getIterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }

}

interface Iterator{
    boolean hasNext();
    Object next();
}

interface Agregate{
    Iterator getIterator();
}

class ArrayTasks implements Agregate{
    String [] tasks = {"Build home", "Kill cat", "Become dev."};

    @Override
    public Iterator getIterator() {
        return new TaskIterator();
    }

    private class TaskIterator implements Iterator{

        int index = 0;

        @Override
        public boolean hasNext() {
            if (index<tasks.length){
                return true;
            }else {
                return false;
            }
        }

        @Override
        public Object next() {
            return tasks[index++];
        }
    }
}

