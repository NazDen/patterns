package mediator;

import java.util.ArrayList;
import java.util.List;

public class MediatorApp {

    public static void main(String[] args) {
        TextChat textChat = new TextChat();

        User admin = new Admin(textChat, "Valera");
        SimpleUser u1 = new SimpleUser(textChat, "Kolian");
        SimpleUser u2 = new SimpleUser(textChat, "Nazar");
        SimpleUser u3 = new SimpleUser(textChat, "Dimon");

        u2.setEnable(false);

        textChat.setAdmin(admin);
        textChat.addUser(u1);
        textChat.addUser(u2);
        textChat.addUser(u3);

        u3.sendMessage("Hello world!");

        admin.sendMessage("Hi everyone)");


    }

}

abstract class User {
    Chat chat;
    String name;
    boolean isEnable = true;

    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean enable) {
        isEnable = enable;
    }

    public User(Chat chat, String name) {
        this.chat = chat;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void sendMessage(String message) {
        chat.sendMessage(message, this);
    }

    abstract void getMessage(String message);
}

interface Chat {
    void sendMessage(String message, User user);
}

class Admin extends User {


    public Admin(Chat chat, String name) {
        super(chat, name);
    }

    @Override
    void getMessage(String message) {
        System.out.println(String.format("Admin %s receives message: %s", getName(), message));
    }
}

class SimpleUser extends User {


    public SimpleUser(Chat chat, String name) {
        super(chat, name);
    }

    @Override
    void getMessage(String message) {
        System.out.println(String.format("User %s receives message: %s", getName(), message));
    }
}

class TextChat implements Chat {
    User admin;
    List<User> users = new ArrayList<>();

    public void setAdmin(User admin) {
        if (admin != null && admin instanceof Admin) {
            this.admin = admin;
        } else {
            throw new RuntimeException("Admin appointed");
        }
    }

    public void addUser(User user) {
        if (admin == null) {
            throw new RuntimeException("Admin didn't appointed");
        } else {
            users.add(user);
        }
    }

    @Override
    public void sendMessage(String message, User user) {
        if (user instanceof Admin) {
            for (User u : users) {
                u.getMessage(message);
            }
        }
        if (user instanceof SimpleUser) {
            for (User u : users) {
                if (u != user && u.isEnable())
                    u.getMessage(message);
            }
            if (admin.isEnable()) {
                admin.getMessage(message);
            }
        }
    }
}