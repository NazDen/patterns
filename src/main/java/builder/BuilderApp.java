package builder;

import enums.MaterialTypes;

public class BuilderApp {

    public static void main(String[] args) {
       CarShop shop =  new CarShop();
       shop.setBuilder(new WoodSubaruBuilder());
       Car car = shop.buildCar();
       System.out.println(car);
       shop.setBuilder(new PlasticNivaBuilder());
       Car car2 = shop.buildCar();
        System.out.println(car2);
    }

}


class Car {
    private String name;
    private MaterialTypes material;
    private int maxSpeed;

    public void setName(String name) {
        this.name = name;
    }

    public void setMaterial(MaterialTypes material) {
        this.material = material;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", material=" + material +
                ", maxSpeed=" + maxSpeed +
                '}';
    }
}

abstract class CarBuilder {

    private String name = "unknown";
    private MaterialTypes material = MaterialTypes.UNKNOWN;
    private int maxSpeed = 0;

    private Car car;

    void createCar() {
        this.car = new Car();
    }

    abstract void buildName();

    abstract void buildMaterial();

    abstract void buildMaxSpeed();

    Car getCar() {
        return car;
    }
}

class WoodSubaruBuilder extends CarBuilder {

    @Override
    void buildName() {
        getCar().setName("Subaru");
    }

    @Override
    void buildMaterial() {
        getCar().setMaterial(MaterialTypes.WOOD);
    }

    @Override
    void buildMaxSpeed() {
        getCar().setMaxSpeed(160);
    }
}

class PlasticNivaBuilder extends CarBuilder {

    @Override
    void buildName() {
        getCar().setName("Niva");
    }

    @Override
    void buildMaterial() {
        getCar().setMaterial(MaterialTypes.PLASTIC);
    }

    @Override
    void buildMaxSpeed() {
        getCar().setMaxSpeed(90);
    }
}

class CarShop{
    private CarBuilder builder;

    public void setBuilder(CarBuilder builder) {
        this.builder = builder;
    }

    public Car buildCar(){
        builder.createCar();
        builder.buildName();
        builder.buildMaterial();
        builder.buildMaxSpeed();
        return builder.getCar();

    }
}