package factory.toys;

public class Lego implements Toy {
    public void play() {
        System.out.println("Playing with lego.");
    }
}
