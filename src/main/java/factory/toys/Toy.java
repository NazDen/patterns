package factory.toys;

public interface Toy {
    void play();
}
