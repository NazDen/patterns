package factory.toys;

public class Car implements Toy {
    public void play() {
        System.out.println("Playing with car.");
    }
}
