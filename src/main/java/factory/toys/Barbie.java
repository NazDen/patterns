package factory.toys;

public class Barbie implements Toy {
    public void play() {
        System.out.println("Playing with barbie.");
    }
}
