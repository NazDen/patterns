package factory;

import enums.ToyTypes;
import factory.toys.*;

public class FactoryApp {
    public static void main(String[] args) {
        ToyFactory factory = new ToyFactoryImp();

        for (ToyTypes type:
             ToyTypes.values()) {
            if (type.equals(ToyTypes.UNKNOWN)){
                continue;
            }
            factory.createToy(type.toString()).play();
        }
    }
}

interface ToyFactory{
    Toy createToy(String toyType);
}

class ToyFactoryImp implements ToyFactory{

    public Toy createToy(String toyType) {
        if (toyType.equalsIgnoreCase(ToyTypes.CAR.name()))
            return  new Car();
        else if (toyType.equalsIgnoreCase(ToyTypes.TRAIN.name()))
            return new Train();
        else if (toyType.equalsIgnoreCase(ToyTypes.LEGO.name()))
            return new Lego();
        else  if (toyType.equalsIgnoreCase(ToyTypes.BARBIE.name()))
            return new Barbie();
        else
            throw new TypeNotPresentException(String.format("%s doesnt available.", toyType), new IllegalArgumentException());
    }
}
